var compressor = require('node-minify');

var documentRoot = './artefact-nexity/';

// With Promise
var vendorsPromise = compressor.minify({
  compressor: 'uglifyjs',
  input: [
    documentRoot + 'js/vendor/jquery-1.11.2.min.js',
    documentRoot + 'js/vendor/jquery-scrolltofixed.js',
    documentRoot + 'js/vendor/imagesloaded.pkgd.min.js',
    documentRoot + 'js/vendor/anime.min.js',
    documentRoot + 'js/vendor/slick.js',
    documentRoot + 'js/vendor/bootstrap.min.js',
    documentRoot + 'js/vendor/bootstrap-modalmanager.min.js',
    documentRoot + 'js/vendor/bootstrap-modal.min.js'
  ],
  output: documentRoot + 'js/vendor.min.js'
});

var mainPromise = compressor.minify({
  compressor: 'uglifyjs',
  input: [documentRoot + 'js/main.js', documentRoot + 'js/contact.js'],
  output: documentRoot + 'js/main.min.js'
});

vendorsPromise.then(function(min) {
  console.log('Vendors merge minify OK');
});

mainPromise.then(function(min) {
  console.log('Main file minify OK');
});
