'use strict';

var API_ENDPOINT = '/gateway.php'; //'https://www.nexity.fr/ws-rest/contact.json';

var ContactLabels = {};
ContactLabels.CALL_ME = 'callme';
ContactLabels.RDV = 'rdv';
ContactLabels.ASK_INFO = 'askinfo';

var MailSubjects = {};
MailSubjects[ContactLabels.CALL_ME] = 'Call me';
MailSubjects[ContactLabels.RDV] = 'Prendre RDV';
MailSubjects[ContactLabels.ASK_INFO] = 'Ask info';

function clickPopin(e) {
  $('#contact_popin').removeClass('success');
  var id = $(e.currentTarget).attr('id');
  $('#contact_popin')
    .find('.modal-title')
    .text($(e.currentTarget).text());

  $('#contactform')
    .find('[name="objet_mail"]')
    .val(MailSubjects[id]);
}

function isCPValid(input) {
  return /((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}/.test(input);
}

function isTelephoneValid(input) {
  return /(((0|(00|\+)33[-. ]?)[1-9]([-. ]?[0-9]{2}){4})|((00|\+)(?!33|0)([-. ]?[0-9])+)+)/.test(
    input
  );
}

function clickSubmit(e) {
  e.preventDefault();
  var selector = $('#contactform');
  selector.find('.input-group,.accept').removeClass('has-error');
  var requiredFields = ['nom', 'prenom', 'telephone', 'codepostal', 'email'];
  var field = '';
  var isValid = true;
  var postData = {};
  var x = selector.serializeArray();
  for (var p = 0; p < x.length; ++p) {
    postData[x[p].name] = x[p].value;
  }
  for (var i = 0; i < requiredFields.length; ++i) {
    if (!postData[requiredFields[i]]) {
      selector
        .find('[name="' + requiredFields[i] + '"]')
        .parents('.input-group')
        .addClass('has-error');
      isValid = false;
    }
  }

  if (isValid) {
    var cpField = selector.find('[name="codepostal"]');
    if (!isCPValid(cpField.val())) {
      isValid = false;
      cpField.parents('.input-group').addClass('has-error');
    }
  }

  if (isValid) {
    var telephoneField = selector.find('[name="telephone"]');
    if (!isTelephoneValid(telephoneField.val())) {
      isValid = false;
      telephoneField.parents('.input-group').addClass('has-error');
    }
  }

  if (!postData.hasOwnProperty('optin_nexity')) {
    selector.find('.accept-nexity').addClass('has-error');
    isValid = false;
  }
  if (!postData.hasOwnProperty('optin_partenaire')) {
    selector.find('.accept-partenaire').addClass('has-error');
    isValid = false;
  }

  if (isValid) {
    $.post(API_ENDPOINT, postData, function(res) {
      console.log(res);
      if (res.status === 'ok') {
        selector.get(0).reset();
        $('#contact_popin').addClass('success');
      } else {
      }
    });
  }
}

function initContact() {
  // looking for ctcsrc
  var qs = document.location.search.split('&');
  var param;
  for (var o = 0; o < qs.length; ++o) {
    param = qs[o].split('=');
    if (param[0].toLowerCase() === 'ctcsrc') {
      $('#contactform')
        .find('[name="ctcsrc"]')
        .val(param[1]);
    }
  }
  // button events
  for (var i in ContactLabels) {
    $('#' + ContactLabels[i]).click(clickPopin);
  }

  //overriding form behaviour
  $('#contactform')
    .find('[type="submit"]')
    .click(clickSubmit);
}

$(document).ready(function() {
  initContact();
});
