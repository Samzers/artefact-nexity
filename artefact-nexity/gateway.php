<?php


header('Content-Type: application/json');
define('ENDPOINT_URL' , 'https://www.nexity.fr/ws-rest/contact.json');

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    exit();
}

$postData  = $_POST;

/*
[
    'objet_mail'=>'Call me',
    'ctcsrc'=>'',
    'nom'=>'test',
    'prenom'=>'test',
    'telephone'=>'0145342310',
    'email'=>'test@testt.com',
    'codepostal'=>'75020',
    'optin_nexity'=>'1',
    'optin_partenaire'=>'0'
];
*/


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, ENDPOINT_URL);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // On dev server only!
$result = curl_exec($ch);


die($result);